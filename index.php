<?php

require('animal.php');
require('frog.php');
require('ape.php');
$object = new Animal("shaun");

echo "Animal Name : $object->name <br>"; // "shaun"
echo "Animal legs : $object->legs <br>"; // 2
echo "Hot blooded animal : $object->hot_blooded <br><br>"; // false

$object2 = new Frog("buduk");

echo "Animal name : $object2->name <br>";
echo "Legs : $object2->legs <br>";
echo "Blood : $object2->cold_blooded <br>";
echo $object2->jump("hop hop");

$object3 = new Ape("Sungokong");

echo "Animal name : $object3->name <br>";
echo "Legs : $object3->legs <br>";
echo "Blood : $object3->hot_blooded <br>";
echo $object3->yell("Auooo");

